# p-peter

`cd proxy && ./genCert.sh; cd ..`

`docker-compose -f FILE up`

- bench-compose.yml uses fake PAC
- docker-compose.yml uses real PAC
- pac-compose.yml is only a PAC
- upstream-compose.yml everything except PAC

If initilization scripts changed, remember to delete the previous db volume

`docker volume rm p-peter_db`

## Scale up

`docker-compose -f FILE scale SERVICE=#REPLICAS`

e.g.

`docker-compose -f bench-compose.yml scale pac=50`
