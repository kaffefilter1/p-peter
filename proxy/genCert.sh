#!/bin/sh

# Create private key
openssl ecparam -genkey -name prime256v1 -out privkey.pem

# Create certficate signing request
openssl req -new -subj "/CN=kaffefilter" -sha256 -key privkey.pem -out csr.csr

# Self-sign certficate
openssl req -x509 -sha256 -key privkey.pem -in csr.csr -out cert.crt

# remove certificate signing reqest
rm csr.csr
